package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
	"sync"
	"time"

	"github.com/go-ini/ini"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

type PipelineStages struct {
	URLs      map[string]map[string]string `json:"_links"`
	ID        string                       `json:"id"`
	Name      string                       `json:"name"`
	Status    string                       `json:"status"`
	StartTime int64                        `json:"startTimeMillis"`
	Duration  int64                        `json:"durationMillis"`
}

type PipelineRunHistory struct {
	URLs          map[string]map[string]string `json:"_links"`
	ID            string                       `json:"id"`
	Name          string                       `json:"name"`
	Status        string                       `json:"status"`
	StartTime     int64                        `json:"startTimeMillis"`
	EndTime       int64                        `json:"endTimeMillis"`
	Duration      int64                        `json:"durationMillis"`
	QueueDuration int64                        `json:"queueDurationMillis"`
	PauseDuration int64                        `json:"pauseDurationMillis"`
	Stages        []PipelineStages
}

type MyError struct {
	When time.Time
	What string
}

type JobCollector struct {
	links   []JobLink
	jobDesc *prometheus.Desc
}

type JobLink struct {
	name     string
	url      string
	user     string
	password string
}

var (
	showVersion = flag.Bool("version", false, "Print version information.")
	buildstamp  = "current"
	githash     = "current"
	cfg         = flag.String("config", "config.ini", "Configuration ini file")
	config      = ini.Empty()
	queueSize   = 10 // How many query users info in parallell
)

func (c *JobCollector) Describe(ch chan<- *prometheus.Desc) {
	ch <- c.jobDesc
}

func (c *JobCollector) Collect(ch chan<- prometheus.Metric) {
	for i := 0; i < len(c.links)/queueSize+1; i++ {
		start := i * queueSize
		end := start + queueSize - 1
		if end >= len(c.links) {
			end = start + len(c.links)%queueSize - 1
		}
		var wg sync.WaitGroup
		for j := start; j <= end; j++ {
			wg.Add(1)
			go func(name string, url string, user string, password string) {
				defer wg.Done()
				jobLastRun, err := getJobStatus(url, user, password)
				if err != nil || jobLastRun == nil {
					return
				}
				var status int64
				if jobLastRun.Status == "SUCCESS" {
					status = 1
				} else {
					status = 0
				}
				for key, val := range map[string]int64{"status": status,
					"startTimeMillis": jobLastRun.StartTime, "endTimeMillis": jobLastRun.EndTime,
					"durationMillis": jobLastRun.Duration, "queueDurationMillis": jobLastRun.QueueDuration,
					"pauseDurationMillis": jobLastRun.PauseDuration} {
					ch <- prometheus.MustNewConstMetric(
						c.jobDesc,
						prometheus.GaugeValue,
						float64(val),
						name,
						key,
						url,
					)
				}
			}(c.links[j].name, c.links[j].url, c.links[j].user, c.links[j].password)
		}
		wg.Wait()
	}
}

func getJobStatus(url string, user string, password string) (*PipelineRunHistory, error) {
	client := &http.Client{
		Timeout: time.Second * 10,
	}
	apiURL := fmt.Sprintf("%s/wfapi/runs", url)
	req, err := http.NewRequest("GET", apiURL, nil)
	if err != nil {
		log.Printf("Error \"%v\"", err)
		return nil, err
	}
	req.SetBasicAuth(user, password)
	resp, err := client.Do(req)
	if err != nil {
		log.Printf("Error \"%v\"", err)
		return nil, err
	}
	if resp.StatusCode < 200 || resp.StatusCode > 299 {
		log.Printf("Fail to get url %s HTTP error %s", apiURL, resp.Status)
		return nil, nil
	}
	body, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		log.Printf("Fail to get json from %s error \"%v\"", apiURL, err)
		return nil, err
	}

	s, err := getStatus([]byte(body))
	if err != nil {
		log.Printf("Fail to get job status from %s error \"%v\"", apiURL, err)
		return nil, err
	}
	return s, nil
}

func (e MyError) Error() string {
	return fmt.Sprintf("%v: %v", e.When, e.What)
}

func getStatus(body []byte) (*PipelineRunHistory, error) {
	var s = make([]PipelineRunHistory, 0)
	err := json.Unmarshal(body, &s)
	if err != nil {
		return nil, err
	}
	for _, rec := range s {
		if rec.Status != "IN_PROGRESS" && rec.Status != "PAUSE_PENDING_INPUT" {
			return &rec, nil
		}
	}
	return nil, &MyError{
		time.Now(),
		"No completed jobs found",
	}
}

func main() {
	flag.Parse()
	if *showVersion {
		fmt.Fprintln(os.Stdout, "Git Commit Hash:", githash)
		fmt.Fprintln(os.Stdout, "Build Time:", buildstamp)
		os.Exit(0)
	}
	config, err := ini.Load(*cfg)
	if err != nil {
		log.Fatalf("Error %v", err)
	}
	port := config.Section("server").Key("port").MustString("8080")
	addr := flag.String("listen-address", ":"+port, "The address to listen on for HTTP requests.")

	jobsLinks := []JobLink{}
	for _, jobName := range config.SectionStrings() {
		if jobName != "DEFAULT" && strings.ToLower(jobName) != "server" {
			jobsLinks = append(jobsLinks, JobLink{
				name:     jobName,
				url:      config.Section(jobName).Key("job_url").Value(),
				user:     config.Section(jobName).Key("user").Value(),
				password: config.Section(jobName).Key("password").Value(),
			})
		}
	}

	prometheus.MustRegister(&JobCollector{
		links: jobsLinks,
		jobDesc: prometheus.NewDesc("job_healthcheck_status", "Jenkins last job status.",
			[]string{
				"name",
				"type",
				"url",
			}, nil),
	})

	http.Handle("/metrics", promhttp.Handler())
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(`<html>
<head><title>Jenkins Jobs Health Exporter</title></head>
<body>
<h1>Jenkins Jobs Health Exporter</h1>
<p><a href=/metrics>Metrics</a></p>
</body>
</html>`))
	})
	log.Printf("Beginning to serve on port %s", *addr)
	log.Fatal(http.ListenAndServe(*addr, nil))
}
