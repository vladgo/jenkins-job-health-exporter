package main

import (
	"log"
	"net/http"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

type JobCollector struct {
	name     string
	url      string
	user     string
	password string
	jobDesc  *prometheus.Desc
}

func (c *JobCollector) Describe(ch chan<- *prometheus.Desc) {
	ch <- c.jobDesc
}

func (c *JobCollector) Collect(ch chan<- prometheus.Metric) {
	value := 300.0 // Your code to fetch the counter value goes here.
	ch <- prometheus.MustNewConstMetric(
		c.jobDesc,
		prometheus.GaugeValue,
		value,
		c.name,
		c.url,
		"status",
	)
	ch <- prometheus.MustNewConstMetric(
		c.jobDesc,
		prometheus.GaugeValue,
		value,
		c.name,
		c.url,
		"duration",
	)
}

func NewJobCollector() *JobCollector {
	return &JobCollector{
		name:     "name1",
		url:      "http://localhost",
		user:     "admin",
		password: "admin",
		jobDesc: prometheus.NewDesc("job_healthcheck_status", "Jenkins last job status.",
			[]string{
				"name",
				"url",
				"type",
			}, nil),
	}
}

func main() {
	prometheus.MustRegister(NewJobCollector())
	http.Handle("/metrics", promhttp.Handler())
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(`<html>
<head><title>Jenkins Jobs Health Exporter</title></head>
<body>
<h1>Jenkins Jobs Health Exporter</h1>
<p><a href=/metrics>Metrics</a></p>
</body>
</html>`))
	})
	log.Printf("Beginning to serve on port %s", "8081")
	log.Fatal(http.ListenAndServe(":8081", nil))
}
