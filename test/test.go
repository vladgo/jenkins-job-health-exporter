package main

import (
	"flag"
	"fmt"
	"time"
)

func hello() {
	fmt.Println("Hello world goroutine")
	fmt.Println(time.Now().Unix())
}

func main() {
	fmt.Println("args", flag.NArg())
	go hello()
	fmt.Println("main function")
	time.Sleep(1 * time.Second)
}
